package com.example.notificationservice.service;

import com.example.notificationservice.entity.CodeVerificationEntity;
import com.example.notificationservice.entity.NotificationHistoryEntity;
import com.example.notificationservice.exceptions.NoSuchEmailException;
import com.example.notificationservice.model.NotificationStatus;
import com.example.notificationservice.model.NotificationType;
import com.example.notificationservice.repository.CodeVerificationRepository;
import com.example.notificationservice.repository.NotificationHistoryRepository;
import jakarta.mail.Message;
import jakarta.mail.internet.InternetAddress;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;

import static java.util.stream.DoubleStream.builder;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailNotificationService {

    private final CodeVerificationRepository notificationRepository;
    private final NotificationHistoryRepository notificationHistoryRepository;
    private final JavaMailSender javaMailSender;

    //    @Value("${jwt.jwtSecret}")
    private final String from = "dikiystore@gmail.com";

    @Transactional
    public void sendVerificationCode(String email) {

        var verificationCode = generateCode();

        String message = "Код подтверждения: ";
        String subject = "Код подтверждения";

        notificationRepository.deleteByEmail(email);

        //TODO создать экземпляр NotificationHistoryEntity с необходимыми данными (status по умолчанию SENT, createdAt принимает LocalDateTime.now()), с помощью builder (не забыть @Builder в сущности)

        NotificationHistoryEntity notificationHistoryEntity = NotificationHistoryEntity.builder()
                .email(email)
                .createdAt(LocalDateTime.now())
                .verificationCode(generateCode())
                .status(NotificationStatus.SENT)
                .type(NotificationType.CODE)
                .message("Пару слов туда сюда")
                .build();
        try {
            sendEmail(email, message + verificationCode, subject);
            notificationRepository.save(new CodeVerificationEntity(email, verificationCode));
            notificationHistoryRepository.save(notificationHistoryEntity);
        } catch (Exception e) {

            notificationHistoryEntity.setStatus(NotificationStatus.SEND_ERROR);
            notificationHistoryRepository.save(notificationHistoryEntity);
        }

    }

    @Transactional
    public void sendMessage(String email, String message) {

        String subject = "Уведомление";
        NotificationHistoryEntity notificationHistoryEntity = NotificationHistoryEntity.builder()
                .email("vittahabaev@gmail.com")
                .createdAt(LocalDateTime.now())
                .verificationCode(generateCode())
                .status(NotificationStatus.SENT)
                .type(NotificationType.MESSAGE)
                .message("Пару слов туда сюда")
                .build();


        try {
            sendEmail(email, message, subject);
            notificationHistoryRepository.save(notificationHistoryEntity);
        } catch (Exception e) {

            notificationHistoryEntity.setStatus(NotificationStatus.SEND_ERROR);
            notificationHistoryRepository.save(notificationHistoryEntity);
        }

    }

    @Transactional
    public Boolean verify(String email, String code) {
        var verification = notificationRepository.findByEmail(email);

        if (verification == null) {
            throw new NoSuchEmailException(email);
        } else {
            if (code.equals(verification.getVerificationCode())) {
                notificationRepository.deleteByEmail(email);
                return true;
            }
            return false;

        }
    }

    public void sendEmail(String email, String message, String subject) {

        MimeMessagePreparator preparator = mimeMessage -> {
            mimeMessage.setFrom(new InternetAddress(from, "DikiyStore"));
            mimeMessage.setContent(message, "text/html; charset=UTF-8");
            mimeMessage.setSubject(subject, "UTF-8");
            mimeMessage.setSentDate(new Date());
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mimeMessage.setHeader("Content-Type", "text/html; charset=UTF-8");
        };
        javaMailSender.send(preparator);
    }

    private String generateCode() {
        return RandomStringUtils.randomNumeric(6);
    }
}
